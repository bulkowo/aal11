# AAL11
  
Dominik Tybura 285743  
Aplikacja na laboratoria z przedmiotu AAL  

Problem Spotkanie Biznesowe  

Prezes Bonifacy organizuje ważne spotkanie firmowe, które zadecyduje o przyszłości firmy.  
Każdy z pracowników w jego firmie zatrudniony jest w dokładnie jednym z n działów oraz może brać udział w więcej niż jednym z m projektów.  
Prezes Bonifacy chce, aby w spotkaniu uczestniczył jeden przedstawiciel dla każdego z projektów,  
ale żeby spotkanie nie zostało zdominowane przez żaden z działów (tzn. osób z jednego działu musi być mniej niż m/2).  
Na podstawie listy przyporządkowującej pracowników do działów i projektów należy opracować algorytm wybierający przedstawicieli na spotkanie.  
Przykładowe dane:  
  Jan HR P1 P2   
  Anna Księgowość P1 P3  
  Wojtek Księgowość P2 P3  
  Ola IT P3  
Prawidłowa odpowiedź  
  Jan P2   
  Anna P1  
  Ola P3  

(
Odpowiedź wyświetla się tylko gdy jest możliwe utworzenie pełnego przepływu, ale jest w postaci:  
W<worker1_id>: P<project_id1>, P<project_id2>,... , D<departament1_id>,...  
W<worker2_id>: P<project_id3>, P<project_id4>,... , D<departament2_id>,...  
)

Wymagane biblioteki pythona:  
* numpy  
* matplotlib  
* networkx  
(umieszczone są w pliku requirements.txt)  

Aby uruchomić główny program, należy sprawdzić help w main.py (python3 main.py --help)  
Aby uruchomić benchmark czasowy algorytmów, należy sprawdzić help w timemeasure.py (python3 timemeasure.py --help)  

Pliki:  

graph.py: zawiera główny interface "solvera" do maksymalnego przepływu grafów oraz klasy wierzchołków i krawędzi  
FF.py: zawiera uzupełnienie interface-u z graph.py o metodę Forda-Fulkersona przy użyciu DFS  
EK.py; zawiera uzupełnienie interface-u z graph.py o algorytm Edmonda-Karpa  
Dinitz.py: zawiera uzupełnienei interface-u z graph.py o algorytm Dinitza (Dinica)  
graphGenerator.py: metody do utworzenia utworzenia grafów  
graphVisualise.py: metody do wizualizacji grafów przy użyciu MatPlotLib i NetworkX  
jsonRunner.py: metody do odczytu i zapisu plików jsonowych w ramach pobierania i zapisywania stworzonych grafów  

main.py: główny rozrusznik algorytmów, nie oblicza czasu, ale pozwala na swobodne obserwacje wyników algorytmów  
timemeasure.py: główny benchmark czasowy, który wyświetla tylko czasy wykonywania podanych zadań  

Kod źródłowy jest skomentowany i w nim znajdują się najważniejsze informacje jak działa jakaś funkcja