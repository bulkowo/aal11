'''
Spotkanie Firmowe AAL 11

File defining Ford Fulkerson method Graph solver
'''
__author__ = 'Dominik Tybura 285743'

import graph as gh
import graphVisualise as gv
import jsonRunner as jr

class FF(gh.Graph):
    '''
    Ford Fulkerson graph solver using Graph as parent class

    In contrast to Edmond Karp's algorithm, FF will use DFS
    '''
    def __init__(self, size):
        super().__init__(size)

    def dfs(self, node, flow):
        '''
        Depth First Search implementation for FF method
        '''

        # return flow instantly if start node is sink
        if node == self.sink_id:
            return flow

        # get all edges of current node and set node as visited
        edges = self.graph[node]
        self.visit_node(node)

        # for every edge of node
        for edge in edges:
            # get remaining capacity of given edge
            remaining = edge.remaining_cap()

            # go further in depth if there is still capacity left and node wasn't visited
            if remaining > 0 and not self.is_visited(edge.end):
                temp_flow = self.dfs(edge.end, min(flow, remaining))

                # update edges flow if flow reached sink and it's positive
                if temp_flow > 0:
                    edge.update(temp_flow)
                    return temp_flow
        return 0

    def solve(self):
        '''
        Function defining main loop for solving Max Flow problem for given graph
        '''
        f = -1
        while f != 0:
            f = self.dfs(self.source_id, 999999)
            self.unvisit_every_node()
            self.max_flow += f

def example1():
    solver = jr.read_from_json('example1.json', FF)
    if solver:
        print(solver.get_max_flow())
        print(solver)
        gv.draw_graph(solver)

def example2():
    # przyklad z koncepcji
    solver = jr.read_from_json('example2.json', FF)
    if solver:
        print(solver.get_max_flow())
        print(solver)
        gv.draw_graph(solver)

if __name__ == '__main__':
    example2()