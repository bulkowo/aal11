'''
Spotkanie Firmowe AAL 11

File defining graph creation functions
'''
__author__ = 'Dominik Tybura 285743'

import jsonRunner as jr
import numpy as np
import random
from itertools import product
import copy
import math

'''
edge density
minimum = min(size of current layer, size of next layer)
maximum = max(size of current layer, size of next layer)

level 0 (source) - level 1: maximum * minimum
level 3 - level 4 (sink): maximum * minimum
rest:
level 1 node needs at least one edge to level 2 nodes (no more constraints):
maximum + k*(minimum-1)*maximum, where k is density value (0 - 1)
level 2 node needs one edge to level 3 nodes
size of level 3 layer <= size of level 2 layer

every node, except source and sink should have edges in and out of layer
'''

def get_level(idx, projects, workers, deps):
    '''
    Function returning level of node
    '''
    if idx < 0 or idx > 2 + projects + workers + deps:
        print('idx out of bounds')
        return -1
    
    if idx == 0:
        return 0
    
    elif idx > 0 and idx <= projects:
        return 1

    elif idx > projects and idx <= projects + workers:
        return 2

    elif idx > projects + workers and idx <= projects + workers + deps:
        return 3

    else:
        return 4

def generate_graph_with_sizes(graph_class, projects, workers, deps, density, many_projects=False):
    if any([projects, workers, deps]) < 0:
        print('Sizes cannot be negative')
        return None 

    if projects > workers and not many_projects:
        print('There is no way to pass every project through if there are less workers than projects')
        return None

    if workers < deps:
        print('Workers size must be bigger than deps size')
        return None

    if density < 0 or density > 1:
        print('Density should be in range 0-1')
        return None
    
    # total size of graph
    graphsize = 2 + projects + workers + deps
    graph = graph_class(graphsize) # source + sink + rest

    # place to hold nodes in lists with corresponding levels
    nodes = [[] for _ in range(5)]

    # add nodes to nodes list and to graph
    for i in range(graphsize):
        level = get_level(i, projects, workers, deps)
        graph.add_node(i, level)
        nodes[level].append(i)

    # 0 - 1
    # connect every node from level 1 to source
    for proj_node in nodes[1]:
        graph.add_edge(nodes[0][0], proj_node, 1)

    # 1 - 2
    # first ensure random connection between every node from level 1 and level 2 to eachother
    # then the randomly assign rest edges 
    # get minimum and maximum amount of nodes and calculate edges_count regarding density param
    maximum, minimum = max(projects, workers), min(projects, workers)
    edges_count = maximum + density * (minimum - 1) * maximum

    # first make sure that every node from lesser has edges
    # by creating copies of node lists and shuffling them
    cpd_nodes = [[], copy.deepcopy(nodes[1]), copy.deepcopy(nodes[2])]
    np.random.shuffle(cpd_nodes[1])
    np.random.shuffle(cpd_nodes[2])

    # get knowledge about which level of nodes contains less amount of nodes
    lesser, bigger = (1, 2) if projects < workers else (2, 1)

    # get product of between all nodes from these levels and shuffle outcome
    prod = list(product(nodes[1], nodes[2]))
    np.random.shuffle(prod)

    cap = 1 # capacity of 1-2 level edge

    # for every node lesser list
    for i in range(len(nodes[lesser])):
        # create tuple of first nodes from shuffled lists
        tpl = (cpd_nodes[1].pop(), cpd_nodes[2].pop())
        # remove from main product list
        prod.remove(tpl)
        # add given tuple (edge) to graph
        graph.add_edge(tpl[0], tpl[1], cap)
        edges_count -= 1
    
    # if there are still nodes left in bigger list of nodes
    if len(cpd_nodes[bigger]) > 0:
        # for every left node try to assign random node from other level
        for i in range(len(cpd_nodes[bigger])):
            # tuple is created differently, depending on which level is bigger
            if bigger == 1:
                tpl = (cpd_nodes[1].pop(), random.choice(nodes[2]))
            else:
                tpl = (random.choice(nodes[1]), cpd_nodes[2].pop())
            # remove from main product list
            prod.remove(tpl)
            # add given tuple (edge) to graph
            graph.add_edge(tpl[0], tpl[1], cap)
            edges_count -= 1

    # if there still edges to be added
    while edges_count > 0:
        # get random tuple from product list
        tpl = prod.pop()
        # remove is not needed as we popped the value
        # add given tuple (edge) to graph
        graph.add_edge(tpl[0], tpl[1], cap)
        edges_count -= 1

    # 2 - 3
    # first ensure random connection bewteen every node from level 2 and level 3 to eachother
    # then the randomly assign rest of edges 

    # first make sure that every node from lesser (deps) has edges
    # by creating copies of node lists and shuffling them
    cpd_nodes = [[], [], copy.deepcopy(nodes[2]), copy.deepcopy(nodes[3])]
    np.random.shuffle(cpd_nodes[2])
    np.random.shuffle(cpd_nodes[3])

    # get product of between all nodes from these levels and shuffle outcome
    prod = list(product(nodes[2], nodes[3]))
    np.random.shuffle(prod)
    
    # capacity on edges varies depending if worker can manage many projects or not
    cap = 999999 if many_projects else 1
    for i in range(len(nodes[3])):
        # create tuple of first nodes from shuffled lists
        tpl = (cpd_nodes[2].pop(), cpd_nodes[3].pop())
        # remove from main product list
        prod.remove(tpl)
        # add given tuple (edge) to graph
        graph.add_edge(tpl[0], tpl[1], cap)
        edges_count -= 1
    
    # if there are worker nodes still left 
    if len(cpd_nodes[2]) > 0:
        # for every left node try to assign random node from deps level
        for i in range(len(cpd_nodes[2])):
            # create tuple of first nodes from shuffled lists
            tpl = (cpd_nodes[2].pop(), random.choice(nodes[3]))
            # remove from main product list
            prod.remove(tpl)
            # add given tuple (edge) to graph
            graph.add_edge(tpl[0], tpl[1], cap)
            edges_count -= 1

    # if there still edges to be added
    while edges_count > 0:
        # get random tuple from product list
        tpl = prod.pop()
        # remove is not needed as we popped the value
        # add given tuple (edge) to graph
        graph.add_edge(tpl[0], tpl[1], cap)
        edges_count -= 1

    # 3 - 4
    # connect every deps node to sink
    # capacity is equal to ceil of half of projects count 
    cap = math.ceil(projects/2.0)
    for dep_node in nodes[3]:
        graph.add_edge(dep_node, nodes[4][0], cap)

    return graph, [len(node) for node in nodes]

def generate_with_proportions(graph_class, n, proj, work, dep, density, many_projects=False):

    if any([proj, work, dep]) < 1:
        print('Proportion cannot be less than 1')
        return None

    if work < dep:
        print('Work prop must be higher than deps one')
        return None

    if not many_projects and proj > work:
        print('Amount of workers must be higher or equal amount of projects')
        return None

    # eg summed = 100 [proj = 40, work = 50, dep = 10]
    summed = proj + work + dep

    if n < math.ceil(summed/dep):
        print('Too little nodes to create graph')
        return None

    projects = round(n * (proj/summed))
    workers = round(n * (work/summed))
    deps = round(n * (dep/summed))

    return generate_graph_with_sizes(graph_class, projects, workers, deps, density)

def random_proportions(many_projects=False):
    proj = 200
    work = np.random.randint(200, 600) if not many_projects else np.random.randint(100, 600)
    dep = np.random.randint(1, 100)
    return (proj, work, dep)

def generate_with_random_proportions(graph_class, n, density, many_projects=False):
    proj, work, dep = random_proportions(many_projects)
    return generate_with_proportions(graph_class, n, proj, work, dep, density, many_projects)