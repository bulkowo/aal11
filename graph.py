'''
Spotkanie Firmowe AAL 11

File defining base classes for solving Max Flow problems
'''
__author__ = 'Dominik Tybura 285743'

class Vertex:
    '''
    Vertex class defining Node in graph
    '''
    def __init__(self, idx, level):
        self.idx = idx

        # level is number of column layer in graph
        # eg. in our problem level = 0 - source, level = 1 - workers, etc.
        self.level = level

    def __getitem__(self, key):
        return self.idx

class Edge:
    '''
    Class defining Edge connecting 2 nodes in graph
    '''

    def __init__(self, start, end, capacity):
        '''
        :param start: start of edge node idx  
        :param end: end of edge node idx  
        :param capacity: capacity of edge  
        :param flow: current flow of edge  
        :param residual: reference to residual edge to given edge  
        '''
        self.start = start
        self.end = end
        self.capacity = capacity
        self.flow = 0
        self.residual = None

    def is_residual(self):
        '''
        Check if given Edge is residual
        '''
        return self.capacity == 0

    def remaining_cap(self):
        '''
        Calculate remaining capacity in Edge
        '''
        return self.capacity - self.flow

    def update(self, value):
        '''
        Update flow in edge
        '''
        self.flow += value
        self.residual.flow -= value

    def set_residual(self, edge):
        '''
        Set reference to residual edge
        '''
        self.residual = edge

    def __str__(self):
        return f'[{self.start}-{self.end}:{self.flow}/{self.capacity}]'

class Graph:
    '''
    Base Graph Solver class defining main functions for dealing with Max Flow problem

    Uses Adjacency List
    '''

    def __init__(self, size):
        '''
        :param size: amount of nodes in graph  
        :param source_id: idx of source node (currently can be only 0)  
        :param sink_id: idx of sink node (currently can be only (graph's size - 1))  
        :param nodes: list of Vertex objects corresponding to nodes in graph  
        :param graph: list of nested lists of Edges (graph[i] - list of edges going out of i node)  
        :param visited: list of visited nodes in graph  
        :param solved: boolean telling if given Graph was solved  
        :param max_flow: max flow of Graph  
        '''
        self.size = size
        self.source_id = 0
        self.sink_id = size - 1

        self.nodes = [None for _ in range(size)]
        self.graph = [[] for _ in range(size)]
        self.visited = [False for _ in range(size)]

        self.solved = False
        self.max_flow = 0

    def add_node(self, idx, level):
        '''
        Add new node (Vertex) to graph's nodes list
        '''
        if idx < 0 or idx >= self.size:
            return

        if level < 0:
            return

        self.nodes[idx] = Vertex(idx, level)

    def add_edge(self, start, end, capacity):
        '''
        Add edge between given nodes [start and end] with given capacity
        '''
        if capacity < 0:
            raise ValueError('Capacity cannot be negative')

        # create main edge and its residual edge
        main_edge = Edge(start, end, capacity)
        residual_edge = Edge(end, start, 0)

        # set residual references
        main_edge.set_residual(residual_edge)
        residual_edge.set_residual(main_edge)
        
        # add edges to correct nodes
        self.graph[start].append(main_edge)
        self.graph[end].append(residual_edge)

    def visit_node(self, node_id):
        '''
        Function setting nodes as visited
        '''
        self.visited[node_id] = True

    def is_visited(self, node_id):
        '''
        Check if node is visited
        '''
        return self.visited[node_id]

    def unvisit_every_node(self):
        '''
        Function used for clearing visited flags from nodes
        '''
        self.visited = [False for _ in range(self.size)]

    def get_max_flow(self):
        '''
        Function solving given graph and returning its Max Flow
        '''
        self.execute()
        return self.max_flow

    def execute(self):
        '''
        Function executing solver if it wasn't used earlier
        '''
        if self.solved:
            return
        self.solved = True
        self.solve()

    def solve(self):
        '''
        To be filled by child classes
        '''
        pass

    def get_answer(self):
        '''
        Answer for given problem
        '''
        level_lists = [[] for _ in range(5)]
        for node in self.nodes:
            level_lists[node.level].append(node)

        source = level_lists[0]
        projects = level_lists[1]
        workers = level_lists[2]
        deps = level_lists[3]
        sink = level_lists[4]

        def pos_in_list(l, idx):
            for i in range(len(l)):
                if l[i].idx == idx:
                    return i

            return -1

        answer = {}

        for i in range(len(workers)):
            worker_projects = []
            for edge in self.graph[workers[i].idx]:
                if self.nodes[edge.end].level == 3 and edge.flow > 0:
                    answer[i] = {'dep': pos_in_list(deps, edge.end), 'projects': []}

                # flow < 0, because its residual
                if self.nodes[edge.end].level == 1 and edge.flow < 0:
                    worker_projects.append(pos_in_list(projects, edge.end))

            try:
                answer[i]['projects'] = worker_projects
            except:
                continue

        outstr = ''
        for k, v in answer.items():
            projstr = ''
            for project in v['projects']:
                projstr += f'P{project}, '
            dep = v['dep']
            outstr += f'W{k}: {projstr}, D{dep} \n'

        print(outstr)


    def __str__(self):
        outstr = ''
        for i in range(len(self.graph)):
            outstr += f'\n{i}: '
            for edge in self.graph[i]:
                if not edge.is_residual():
                    outstr += f'{str(edge)}, '
        return outstr