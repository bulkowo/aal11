'''
Spotkanie Firmowe AAL 11

File defining Dinitz algorithm Graph solver
'''
__author__ = 'Dominik Tybura 285743'

import graph as gh
from collections import deque
import graphVisualise as gv
import jsonRunner as jr

class Dinitz(gh.Graph):
    '''
    Edmond Karp's graph solver using Graph as parent class

    Using FF method's main idea of finding augmenting paths by 
    leveling graph using BFS and then finding augmenting paths by DFS
    '''
    def __init__(self, size):
        super().__init__(size)

        # level of node (distance in jumps from source)
        self.level = [-1 for _ in range(size)]

    def bfs(self):
        '''
        Breadth First Search implementation for Dinitz algorithm to get levels of nodes
        '''
        # reset all node levels and set source's level as 0
        self.level = [-1 for _ in range(self.size)]
        self.level[self.source_id] = 0

        q = deque()

        # add source to deque
        q.append(self.source_id)

        # while there is node in deque
        while q:
            # pop first node in queue
            node = q.popleft()

            # for every edge in given node
            for edge in self.graph[node]:

                # if there is still remaining capacity and level wasn't set yet
                if edge.remaining_cap() > 0 and self.level[edge.end] == -1:
                    self.level[edge.end] = self.level[node] + 1
                    q.append(edge.end)

        # return bool defining whether sink was reached or not
        return self.level[self.sink_id] != -1

    def dfs(self, node, nxt, flow):
        '''
        Depth First Search implementation for Dinitz method
        '''
        # return flow if given node is sink
        if node == self.sink_id:
            return flow

        # get amount of edges for given node
        edges_count = len(self.graph[node])

        # while there are still edges left
        while nxt[node] < edges_count:
            # get next edge of node
            edge = self.graph[node][nxt[node]]

            # calculate remaining capacity of Edge
            remaining = edge.remaining_cap()

            # if there is still remaining capacity and next edge's level is one higher 
            # (one jump closer to sink according to BFS)
            if remaining > 0 and self.level[edge.end] == self.level[node] + 1:

                # try to reach sink and calculate possible flow to it
                temp_flow = self.dfs(edge.end, nxt, min(flow, remaining))

                # update edges flow if there is flow to sink
                if temp_flow > 0:
                    edge.update(temp_flow)
                    return temp_flow
            nxt[node] += 1

        return 0

    def solve(self):
        '''
        Function defining main loop for solving Max Flow problem for given graph
        '''
        # level graph using BFS as long as it is possible to get to sink
        while self.bfs():
            # list of next edges, used for pruning dead ends during DFS
            # if there was once dead end occured in leveled graph, it should be omitted in next iteration
            # as long leveled graph wasn't reseted by BFS
            nxt = [0 for _ in range(self.size)]

            f = -1
            while f != 0:
                # try to get as much flow as possible while there is still flow after using DFS
                f = self.dfs(self.source_id, nxt, 999999)
                self.max_flow += f

def example1():
    solver = jr.read_from_json('example1.json', Dinitz, True)
    if solver:
        print(solver.get_max_flow())
        print(solver)
        gv.draw_graph(solver)

def example2():
    # przyklad z koncepcji
    solver = jr.read_from_json('example2.json', Dinitz, True)
    if solver:
        print(solver.get_max_flow())
        print(solver)
        gv.draw_graph(solver)

if __name__ == '__main__':
    example2()
