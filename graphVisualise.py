'''
Spotkanie Firmowe AAL 11

File defining functions for visualising graph with its node labels using matplotlib and networkx
'''
__author__ = 'Dominik Tybura 285743'

import networkx as nx 
import matplotlib.pyplot as plt
import graph as gh

def map_graph_to_nx(graph):
    '''
    Function to map Graph structure to NetworkX DiGraph Object

    Used for printning graph using NetworkX draw functions

    :returns: DiGraph or None
    '''
    G = nx.DiGraph()

    # return None if given argument is not Graph
    if not isinstance(graph, gh.Graph):
        return None

    # get tuple of nodes and add it to NetworkX DiGraph
    nodes = tuple(i for i in range(graph.size))
    G.add_nodes_from(nodes)

    # map edges to NetworkX Digraph for every node in Graph 
    for node in nodes:
        edges = graph.graph[node]
        for edge in edges:
            if not edge.is_residual():
                G.add_edge(edge.start, edge.end, capacity=edge.capacity)

    return G

def draw_graph(graph):
    '''
    Function used to draw Graph using NetworkX draw function

    :returns: None
    '''

    # return if given argument is not Graph
    if not isinstance(graph, gh.Graph):
        return

    # get DiGraph
    G = map_graph_to_nx(graph)
    if not G:
        return
    
    # node position mapping in graph
    pos={}

    # if there are nodes defined in Graph structure, try to set nodes in level columns
    if graph.nodes[0]:
        levelRepeats = 0
        lastLevel = -1
        maxLevel = graph.nodes[graph.size-1].level

        start_x = -0.9
        end_x = 0.9
        x_step = (end_x - start_x) / maxLevel

        start_y = -0.9
        end_y = 0.9
        y_step = (end_y - start_y) / (len(graph.nodes) / maxLevel)

        # for every node in graph
        for node in graph.nodes:
            # get current level of node
            currentLevel = node.level

            # raise repeat counter if level is repeating
            if currentLevel == lastLevel:
                levelRepeats += 1
            else:
                lastLevel = currentLevel
                levelRepeats = 0

            # if node is source or sink, set it in predefined slot
            if node.idx == 0:
                pos[node.idx] = [-0.9, -0.4]
            elif node.idx == graph.size - 1:
                pos[node.idx] = [0.9, -0.4]

            # else define its position using below formula
            else:
                pos[node.idx] = [x_step * currentLevel + start_x, y_step * levelRepeats + start_y]

    # if there are not nodes defined, use Spring Layout of NetworkX
    else:
        pos = nx.spring_layout(G)

    nx.draw(G, pos, with_labels=True)
    plt.show()