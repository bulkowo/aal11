
__author__ = 'Dominik Tybura 285743'

import argparse
import graph as gh
from FF import FF
from EK import EK
from Dinitz import Dinitz
import graphGenerator as gg
import graphVisualise as gv
import jsonRunner as jr

parser = argparse.ArgumentParser(
    description=
'''
Program rozwiązujący problem Spotkanie Biznesowe na AAL 19L

Wykorzystuje on sieć przepływową wraz z wykorzystaniem 3 algorytmów*:
- Ford-Fulkerson-a (*faktycznie to jest metoda) [w tym przypadku wykorzystuje DFS]
- Edmond-Karp-a
- Dinitz-a

Przykłady:
python3 main.py --alg FF --gen insertprops -n 100 -d 0.2 10 20 5
python3 main.py --alg Dinitz --gen loadjson --file example1.json
python3 main.py --alg EK --gen randprops -n 100 -d 0.2
python3 main.py --alg FF --gen insertnodes -d 0.2 10 30 7
''',
    formatter_class=argparse.RawTextHelpFormatter
)

parser.add_argument('--alg', dest='algorithm', type=str, default='FF', help='Algorithm to use. Possiblities: FF, EK, Dinitz. Default: FF')
parser.add_argument('--gen', dest='gentype', type=str, default='randprops', help=
'''Type of graph generation
Possibilites:
- randprops: random proportions between projects, workers and departaments, type only size of graph
- loadjson: load json from file after --file flag
- insertprops: type proportions (integers above or equal 1) between projects, workers and departaments and give size of graph
- insertnodes: type node counts for projects, workers and departaments
Default: randprops
'''
)
parser.add_argument('-n', dest='node_count', type=int, default=0, help='Amount of nodes in graph (except source and sink). Default: 100')
parser.add_argument('-d', dest='density', type=float, default=0, help='Density of edges between Level 1 and Level 2 of graph (0 = minimum, 1 = maximum). Default: 0.5')
parser.add_argument('--file', dest='filename', type=str, default='', help='Path to json file with example graph')
parser.add_argument('values', metavar='N', type=int, nargs='*', help=
'''
Rest of data depending on generation type:
- randprops: None
- loadjson: None
- insertprops: <command> a b c; a = proportion of projects, b = proportion of workers, c = proportion of deps
- insertnodes: <command> a b c; a = amount of projects, b = amount of workers, c = amount of deps
'''
)
parser.add_argument('--draw', dest='draw', action='store_true', help='Set to True if want to draw on Matplotlib')
parser.set_defaults(draw=False)
parser.add_argument('--many', dest='many', action='store_true', help='Define if worker might attend meeting with many projects')
parser.set_defaults(many=False)
args = parser.parse_args()

algos = {
    'FF': FF,
    'EK': EK,
    'Dinitz': Dinitz
}

gentypes = [
    'randprops',
    'loadjson',
    'insertprops',
    'insertnodes'
]

def main():
    if args.algorithm not in algos:
        print('Unknown algorithm')
        return

    if args.gentype not in gentypes:
        print('Unknown generation type')
        return

    if args.gentype == 'randprops':
        if args.values or args.filename:
            print('Explicit usage of positional arguments')
            return

        graph, nodes = gg.generate_with_random_proportions(
            graph_class = algos[args.algorithm],
            n = args.node_count,
            density = args.density,
            many_projects= args.many,
        )

    if args.gentype == 'loadjson':
        if args.values or args.node_count or args.density:
            print('Explicit usage of positional arguments')
            return

        graph, nodes = jr.read_from_json(
            graph_class = algos[args.algorithm],
            filename = args.filename, 
            many_projects = args.many,
        )

    if args.gentype == 'insertprops':
        if args.filename:
            print('Explicit usage of positional arguments')
            return

        if len(args.values) != 3:
            print('There should be 3 proportions (projects, workers, deps)')
            return

        graph, nodes = gg.generate_with_proportions(
            graph_class = algos[args.algorithm],
            n = args.node_count,
            proj = args.values[0],
            work = args.values[1],
            dep = args.values[2],
            density = args.density,
            many_projects = args.many,
        )

    if args.gentype == 'insertnodes':
        if args.filename or args.node_count:
            print('Explicit usage of positional arguments')
            return

        if len(args.values) != 3:
            print('There should be 3 node counts (projects, workers, deps)')
            return

        graph, nodes = gg.generate_graph_with_sizes(
            graph_class = algos[args.algorithm],
            projects = args.values[0],
            workers = args.values[1],
            deps = args.values[2],
            density = args.density,
            many_projects = args.many,
        )

    if graph:
        max_flow = graph.get_max_flow()
        #print(graph)
        print(f'\n{max_flow}')
        
        print(f'\nIs it possible: {max_flow == nodes[1]}')
        if max_flow == nodes[1]:
            graph.get_answer()
            
        if args.draw:
            gv.draw_graph(graph)
                

if __name__=='__main__':
    main()


