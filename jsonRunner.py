'''
Spotkanie Firmowe AAL 11

File defining functions for creating graph from and saving graphs to json files
'''
__author__ = 'Dominik Tybura 285743'

import json
from copy import deepcopy as dcp
import graph as gh
import math

'''
{
    '0': {ends: [1,2,3,...], level:0},
    ...
    'n-1': {ends: [], level:4}
}

level 0: source
level 1: projects
level 2: workers
level 3: departaments
level 4: sink
'''

def read_from_json(filename, graph_class, many_projects=False):
    '''
    Read graph data from JSON and process it to given solver  
    many_projects param tells whether worker can have many projects on him or not
    '''

    with open(filename, 'r') as f:
        json_dict = json.load(f)

    if not isinstance(json_dict, dict):
        print('JSON is not dictionary')
        return None

    # amount of nodes in every level of graph
    lvls_occurances = [0, 0, 0, 0, 0]
    for k, v in json_dict.items():
        if not isinstance(v, dict):
            print('Node metadata is not in dictionary')
            return None

        try:
            if not isinstance(v['ends'], (list, tuple)):
                print('Node ends data is not list')
                return None

            if isinstance(v['level'], int):
                if v['level'] >= 0 or v['level'] <= 4:
                    lvls_occurances[v['level']] += 1
                else:
                    print('Node level is out of bounds')
                    return None
            else:
                print('Node level is not int')
                return None
        
        except KeyError:
            print('There is no ends or level key in node dict')
            return None

    # there should be only one source, one sink, and at least one node in the rest of levels
    if any(lvls_occurances) == 0 or lvls_occurances[0] != 1 or lvls_occurances[4] != 1:
        print('Incorrect level distribution')
        return None

    graph = graph_class(len(json_dict))

    # get limit of workers from same departament
    projects_num = lvls_occurances[1]
    m = math.ceil(projects_num/2.0)

    # get source and sink of graph
    source = json_dict[str(0)]
    sink = json_dict[str(graph.size - 1)]

    for i in range(0, len(json_dict)):
        
        # add new Vertex object to graph's node list
        graph.add_node(i, json_dict[str(i)]['level'])
        if i == graph.size - 1:
            break

        i = str(i)
        if json_dict[i]['level'] in [2, 3] and len(json_dict[i]['ends']) != 1:
            print('Workers and Departaments have only one end')
            return None

        # for every given End node
        for end in json_dict[i]['ends']:
            end = str(end)
            
            # next node should have level higher by one regarding current node
            if (json_dict[i]['level'] + 1) != json_dict[end]['level']:
                print('Levels mismatch')
                return None

            # add infinity capacity edge if worker may represent many projects between Worker and Departament
            if many_projects and json_dict[i]['level'] == 2:
                graph.add_edge(int(i), int(end), 9999999)
            
            # add edge with limit of workers capacity for edge between Departament and Sink
            elif json_dict[i]['level'] == 3:
                graph.add_edge(int(i), int(end), m)

            # else add edge with 1 capacity between given nodes
            else:
                graph.add_edge(int(i), int(end), 1)

    return graph, lvls_occurances

def save_to_json(filename, graph):
    '''
    Function used for saving given graph (only nodes and its connections, without capacity) to json
    '''

    with open(filename, 'w') as f:
        out_json = {}
        for node in graph.nodes:
            edges = []
            for edge in graph.graph[node.idx]:
                if not edge.is_residual():
                    edges.append(edge.end)
            out_json[node.idx] = {'ends': edges, 'level': node.level}

        json.dump(out_json, f, indent=2)

