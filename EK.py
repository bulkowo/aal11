'''
Spotkanie Firmowe AAL 11

File defining Edmond Karp's algorithm Graph solver
'''
__author__ = 'Dominik Tybura 285743'

import graph as gh
from collections import deque
import graphVisualise as gv
import graphGenerator as gg
import jsonRunner as jr

class EK(gh.Graph):
    '''
    Edmond Karp's graph solver using Graph as parent class

    Using FF method's main idea of finding augmenting paths using BFS 
    '''
    def __init__(self, size):
        super().__init__(size)

    def bfs(self):
        '''
        Breadth First Search implementation for EK algorithm
        '''
        # list defining reverse path for given node
        # previous[node_2] = edge(node_1, node_2) means that in found path for node_2 previous one was node_1
        previous = [None for _ in range(self.size)]

        q = deque()

        # visit source and add it to deque
        self.visit_node(self.source_id)
        q.append(self.source_id)

        # while there are nodes left in deque
        while q:
            # pop first node in queue
            node = q.popleft()

            # if its sink, it means we found augmenting path
            if node == self.sink_id:
                break

            # for every edge in given node
            for edge in self.graph[node]:

                # if there is still remaining capacity and edge wasn't yet visited
                if edge.remaining_cap() > 0 and not self.is_visited(edge.end):
                    self.visit_node(edge.end)
                    previous[edge.end] = edge
                    q.append(edge.end)

        # if there is no path to sink, it means that there was no augmenting path
        if previous[self.sink_id] == None:
            return 0

        temp_flow = 999999

        # starting from sink, go back to source
        edge = previous[self.sink_id]
        while edge:
            # calculate temporary flow (minimum of what can be reached)
            temp_flow = min(temp_flow, edge.remaining_cap())
            edge = previous[edge.start]

        # starting from sink, go back to source
        edge = previous[self.sink_id]
        while edge:
            # update every edge on augmenting path by temporary flow
            edge.update(temp_flow)
            edge = previous[edge.start]

        # return added flow
        return temp_flow

    def solve(self):
        '''
        Function defining main loop for solving Max Flow problem for given graph
        '''
        flow = 0

        while True:
            self.unvisit_every_node()
            flow = self.bfs()
            self.max_flow += flow

            if flow == 0:
                break

def example1():
    #solver = jr.read_from_json('example1.json', EK)
    solver, nodes = gg.generate_with_random_proportions(EK, 100, 0.2)
    if solver:
        print(solver.get_max_flow())
        print(solver)
        gv.draw_graph(solver)

def example2():
    # przyklad z koncepcji
    solver = jr.read_from_json('example2.json', EK)
    if solver:
        print(solver.get_max_flow())
        print(solver)
        #gv.draw_graph(solver)

if __name__ == '__main__':
    example1()