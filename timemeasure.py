
__author__ = 'Dominik Tybura 285743'

import argparse
from FF import FF
from EK import EK
from Dinitz import Dinitz
import graphGenerator as gg
import time

parser = argparse.ArgumentParser(
    description=
'''
Program generujący benchmarki czasowe dla problemu Spotkanie Biznesowe na AAL 19L

Wykorzystuje on sieć przepływową wraz z wykorzystaniem 3 algorytmów*:
- Ford-Fulkerson-a (*faktycznie to jest metoda) [w tym przypadku wykorzystuje DFS]
- Edmond-Karp-a
- Dinitz-a

Przykład:
python3 timemeasure.py --alg EK -n 100 -s 10 -k 20 -d 0.2 --instances 1
''',
    formatter_class=argparse.RawTextHelpFormatter
)

parser.add_argument('--alg', dest='algorithm', type=str, default='FF', help='Algorithm to use. Possiblities: FF, EK, Dinitz. Default: FF')
parser.add_argument('-n', dest='start_count', type=int, default=0, help='Amount of nodes in graph (except source and sink). Default: 100')
parser.add_argument('-s', dest='node_step', type=int, default=0, help='Step in amount of nodes')
parser.add_argument('-k', dest='iter', type=int, default=0, help='Problems amount')
parser.add_argument('--instances', dest='instances', type=int, default=0, help='Instances of problem amount')
parser.add_argument('-d', dest='density', type=float, default=0, help='Density of edges between Level 1 and Level 2 of graph (0 = minimum, 1 = maximum). Default: 0.5')
parser.add_argument('--many', dest='many', action='store_true', help='Define if worker might attend meeting with many projects')
parser.set_defaults(many=False)
args = parser.parse_args()

algos = {
    'FF': FF,
    'EK': EK,
    'Dinitz': Dinitz
}

def main():
    if not args.density:
        print('You must set every flag to process further')
        return

    if args.algorithm not in algos:
        print('Unknown algorithm')
        return

    if args.start_count <= 0 or args.node_step <= 0 or args.instances <= 0 or args.iter <= 0:
        print('Flags must be positive')
        return

    times = [[] for _ in range(args.instances)]

    for instance in range(args.instances):
        proportions = gg.random_proportions(many_projects=args.many)

        for iteration in range(args.iter):
            t = time.process_time()

            graph = gg.generate_with_proportions(
                graph_class = algos[args.algorithm],
                n = args.start_count + args.node_step * iteration,
                proj = proportions[0],
                work = proportions[1],
                dep = proportions[2],
                density = args.density,
                many_projects = args.many,
            )
            
            elapsed = time.process_time() - t
            times[instance].append(elapsed*1000)
    import pprint as pp

    pp.PrettyPrinter(indent=2)
    pp.pprint(times)

if __name__=='__main__':
    main()


